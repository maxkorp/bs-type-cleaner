#!/usr/bin/env node
const cp = require('child_process');
const fs = require('fs');
const prettier = require('prettier');

const argv = process.argv.slice(2);
if (argv.length == 0) {
  console.log('please pass in one or more files to clean');
}

function clean(filename) {
  const toPretty = 'const type = ' +
    fs.readFileSync(filename)
      .toString()
      .replace('const type = ', '')
      .split('=>')
      .shift()
      .replace(/Relude\.Globals\.Option\.[a-zA-Z]+\.t/g, 'option')
      .replace(/\[\</g, '[')
      .replace(/\|/g, ',')
      .replace(/`/g, '')
      .replace(/\{\.\./g, '{')
      .replace(/\{\./g, '{')
      .replace(/as '[a-z]+/g, '')
      .replace(/let [a-zA-Z]+:/g, '')
      .replace(/Js.t\(/g, '(')
      .replace(/Js.Array.t/g, 'array');

  const cleaned = prettier.format(toPretty, {parser: "babel" });
  fs.writeFileSync(filename, cleaned);
}

argv.forEach(clean);
